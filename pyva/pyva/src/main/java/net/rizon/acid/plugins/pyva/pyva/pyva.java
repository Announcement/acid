package net.rizon.acid.plugins.pyva.pyva;

import com.google.common.eventbus.Subscribe;
import io.netty.util.concurrent.ScheduledFuture;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.plugins.Plugin;
import net.rizon.acid.core.User;
import net.rizon.acid.events.EventCTCP;
import net.rizon.acid.events.EventCTCPReply;
import net.rizon.acid.events.EventChanMode;
import net.rizon.acid.events.EventInvite;
import net.rizon.acid.events.EventJoin;
import net.rizon.acid.events.EventKick;
import net.rizon.acid.events.EventNotice;
import net.rizon.acid.events.EventPart;
import net.rizon.acid.events.EventPrivmsg;
import net.rizon.acid.events.EventQuit;
import net.rizon.acid.events.EventServerNotice;
import net.rizon.acid.events.EventSync;
import net.rizon.acid.events.EventUserConnect;
import net.rizon.acid.plugins.pyva.pyva.conf.Config;
import net.rizon.pyva.Pyva;
import net.rizon.pyva.PyvaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class PyvaEvent
{
	@Subscribe
	public void onUserConnect(EventUserConnect event)
	{
		User u = event.getUser();
		
		this.call("onUserConnect", u);
	}

	@Subscribe
	public void onInvite(EventInvite event)
	{
		User inviter = event.getInviter();
		User invitee = event.getInvitee();
		Channel channel = event.getChannel();

		this.call("onInvite", inviter, invitee, channel);
	}

	@Subscribe
	public void onJoin(EventJoin event)
	{
		for (User u : event.getUsers())
			this.call("onJoin", u, event.getChannel().getName());
	}

	@Subscribe
	public void onPart(EventPart event)
	{
		User user = event.getUser();
		Channel channel = event.getChannel();

		this.call("onPart", user.getNick(), channel.getName());
	}

	@Subscribe
	public void onKick(EventKick event)
	{
		String kicker = event.getKicker();
		User victim = event.getVictim();
		Channel channel = event.getChannel();
		String reason = event.getReason();

		this.call("onKick", kicker, victim, channel.getName(), reason);
	}

	@Subscribe
	public void onQuit(EventQuit event)
	{
		User user = event.getUser();
		String msg = event.getMsg();

		this.call("onQuit", user, msg);
	}

	@Subscribe
	public void onPrivmsg(EventPrivmsg event)
	{
		String creator = event.getCreator();
		String recipient = event.getRecipient();
		String msg = event.getMsg();

		creator = User.toName(creator);
		recipient = User.toName(recipient);
		this.call("onPrivmsg", creator, recipient, msg);
	}

	@Subscribe
	public void onNotice(EventNotice event)
	{
		String creator = event.getCreator();
		String recipient = event.getRecipient();
		String msg = event.getMsg();
		
		creator = User.toName(creator);
		recipient = User.toName(recipient);
		this.call("onNotice", creator, recipient, msg);
	}

	@Subscribe
	public void onServerNotice(EventServerNotice event)
	{
		String source = event.getSource();
		String recipient = event.getRecipient();
		String msg = event.getMsg();

		recipient = User.toName(recipient);
		this.call("onServerNotice", source, recipient, msg);
	}

	@Subscribe
	public boolean eventCTCP(EventCTCP event)
	{
		String creator = event.getCreator();
		String recipient = event.getRecipient();
		String msg = event.getMsg();

		this.call("onCtcp", creator, recipient, msg);
		return false;
	}

	@Subscribe
	public void onCTCPReply(EventCTCPReply event)
	{
		User source = event.getSource();
		String target = event.getTarget();
		String message = event.getMessage();

		this.call("onCtcpReply", source.getUID(), target, message);
	}

	@Subscribe
	public void onSync(EventSync event)
	{
		this.call("onSync");
	}

	@Subscribe
	public void onChanModes(EventChanMode event)
	{
		String prefix = event.getPrefix();
		Channel chan = event.getChan();
		String modes = event.getModes();

		this.call("onChanModes", prefix, chan.getName(), modes);
	}

	private void call(String name, Object... params)
	{
		try
		{
			pyva.call(name, params);
		}
		catch (Exception e)
		{
			pyva.log.warn("Error executing pyva call", e);
		}
	}
}

class GCTimer implements Runnable
{
	@Override
	public void run()
	{
		System.gc();
		pyva.pyva.gc();
	}
}

public class pyva extends Plugin
{
	protected static final Logger log = LoggerFactory.getLogger(pyva.class);

	private Config conf;
	private ScheduledFuture gcTimer;
	public static Pyva pyva;

	private PyvaEvent eventHandlers;

	@Override
	public void start() throws Exception
	{
		reload();

		pyva = new Pyva();

		for (String path : conf.path)
			pyva.addToSystemPath(path);

		pyva.init();

		try
		{
			for (String plugin : conf.plugins)
				loadPyvaPlugin(plugin);
		}
		catch (Exception ex)
		{
			pyva.stop();
			throw ex;
		}
		
		eventHandlers = new PyvaEvent();
		Acidictive.eventBus.register(eventHandlers);
                
		gcTimer = Acidictive.scheduleWithFixedDelay(new GCTimer(), 1, TimeUnit.MINUTES);
	}

	@Override
	public void stop()
	{
		gcTimer.cancel(true);

		Acidictive.eventBus.unregister(eventHandlers);

		try
		{
			for (String plugin : conf.plugins)
				unloadPyvaPlugin(plugin);
		}
		catch (PyvaException e)
		{
			log.error("Unable to unload pyva plugins", e);
		}

		pyva.stop();
	}

	@Override
	public void reload() throws Exception
	{
		conf = (Config) net.rizon.acid.conf.Config.load("pyva.yml", Config.class);

		Acidictive.loadClients(this, conf.clients);
	}

	public static void loadPyvaPlugin(String name) throws PyvaException
	{
		pyva.invoke("plugin", "loadPlugin", name);
	}

	public static void unloadPyvaPlugin(String name) throws PyvaException
	{
		pyva.invoke("plugin", "unloadPlugin", name);
	}

	public static String[] getPyvaPlugins() throws PyvaException
	{
		Object[] obj = (Object[]) pyva.invoke("plugin", "getPlugins");
		String[] stringArray = Arrays.copyOf(obj, obj.length, String[].class);
		return stringArray;
	}

	public static void call(String name, Object... args) throws PyvaException
	{
		Object[] object = new Object[args.length + 1];

		/* the first argument is the function, everything else is its parameters */
		object[0] = name;
		for (int i = 0; i < args.length; ++i)
			object[i + 1] = args[i];

		pyva.invoke("plugin", "call", object);
	}
}
