# part of psm_trivia
# provides the Trivia class for channels
# Also I hate trivia bots.

import string
from datetime import datetime, timedelta
import task

class Trivia(object):
	# state vars:
	# constructor:
	## string cname: channel name
	## PSModule mod: the module calling us
	## string theme: the name of the theme table
	## int question_number: the number of questions to ask (if 0, unlimited)
	# 
	# initialized elsewhere:
	# string[] answer: the answer to the question being asked
	# int asked_questions: the amount of questions already asked
	# int hints_given: the number of hints already given; reset to 0 by ask and
	#                  give_hint.
	# string winner: winner of the last round
	# int streak: number of times the winner responded to a question
	# datetime started: time when we asked the question
	asked_questions = 0
	hints_given = 0
	winner = ""
	streak = 0
	started = None

	# XXX
	themes = {
	'anime': 'Anime',
	'default': 'default',
	'geography': 'Geography',
	'history': 'History',
	'lotr-books': 'LOTR-Books',
	'lotr-movies': 'LOTR-Movies',
	'movies': 'Movies',
	'naruto': 'Naruto',
	'sciandnature': 'ScienceAndNature',
	'simpsons': 'Simpsons',
	'sg1qs': 'Stargate'
	}

	def __init__(self, cname, mod, theme, qnum):
		self.cname = cname
		self.module = mod
		self.theme = theme
		self.question_number = qnum
		self.dbp = mod.dbp

		# Not repeating so people don't have to wait until forever
		# between answered questions
		self.question_timer = task.LoopingCall(self.ask)
		self.question_timer.start(3, False)
		if qnum == 0:
			self.module.msg(self.cname, "Starting round of trivia. Questions: unlimited")
		else:
			self.module.msg(self.cname, "Starting round of trivia. Questions: %d" % qnum)
	
	def ask(self):
		'''Asks a question to the channel.'''
		if self.question_number > 0:
			if self.asked_questions >= self.question_number:
				# call parent to remove reference from dict so GC cleans us up
				self.module.stop_trivia(self.cname, False)
				return

		try:
			query = "SELECT tq.question, tq.answer FROM `trivia_questions` AS tq JOIN (SELECT tq2.id FROM `trivia_questions` AS tq2 JOIN `trivia_themes` AS tt ON tq2.theme_id=tt.theme_id AND tt.theme_name='%s' ORDER BY rand() LIMIT 1) AS x WHERE tq.id = x.id"
			self.dbp.execute(query % self.themes[self.theme])
		except Exception, ex:
			self.module.elog.error('Unable to look up trivia question: %s' % ex)
			return
		resultset = self.dbp.fetchone()
		# If there is a *, multiple answers may be possible. Silly Trivia DB doing
		# silly things.
		self.answer = unicode(resultset[1]).split('*')

		self.asked_questions += 1
		self.hints_given = 0

		self.hint_timer = task.LoopingCall(self.give_hint)
		self.hint_timer.start(10, False)

		# SOME questions have a question mark already appended, some don't.
		# We're gonna keep compatibility here and append a question mark.
		self.module.msg(self.cname, "%d. %s?" % (self.asked_questions, resultset[0]))
		self.started = datetime.now()

	def give_hint(self):
		'''Gives a hint, increases the hints_given variable and sets timer for
		the next hint being given.'''
		factor = 0
		end = 0

		if self.hints_given == 0:
			factor = 0.25
			self.hint_timer = task.LoopingCall(self.give_hint)
			self.hint_timer.start(10, False)
		elif self.hints_given == 1:
			if len(self.answer[0]) < 20:
				factor = 0.5
			else:
				factor = 0.4
			self.hint_timer = task.LoopingCall(self.give_hint)
			self.hint_timer.start(20, False)
		# Also show some parts of the ending if last hint
		elif self.hints_given == 2:
			if len(self.answer[0]) < 20:
				factor = 0.5
			else:
				factor = 0.4
			if len(self.answer[0]) >= 32:
				end = 7
			elif len(self.answer[0]) >= 24:
				end = 5
			elif len(self.answer[0]) >= 16:
				end = 3
			elif len(self.answer[0]) >= 8:
				end = 2
			elif len(self.answer[0]) >= 4:
				end = 1
			self.hint_timer = task.LoopingCall(self.give_hint)
			self.hint_timer.start(20, False)
		elif self.hints_given == 3:
			#End the question!
			self.module.msg(self.cname, "Time's up! The answer was: " + self.answer[0])
			self.answer = []
			self.hints_given = 0
			self.hint_timer = task.LoopingCall(self.ask)
			self.hint_timer.start(5, False)
			self.streak = 0
			return

		# Length from the beginning of the string to be shown
		length = (int)(len(self.answer[0]) * factor)
		# substring from the beginning to length to be shown
		showstr = self.answer[0][0:length]
		# substring from the middle of the string to be hidden
		hidestr = ""
		# if "end", the number of characters to show at the end is not 0 only go until there
		if end > 0:
			hidestr = self.answer[0][length:len(self.answer[0])-end]
		else:
			hidestr = self.answer[0][length:]
		tltable = dict(zip(map(ord, u'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'), map(ord, u'**************************************************************')))
		hidestr = hidestr.translate(tltable)
		# substring to be shown if there's an end
		endstr = ""
		if end > 0:
			endstr = self.answer[0][len(self.answer[0])-end:]
		# Hint to be sent
		hint = showstr + hidestr + endstr
		self.module.msg(self.cname, "Hint: " + hint)

		self.hints_given += 1
	
	def check_answer(self, nick, answer):
		'''checks if a given answer is the one we're looking for. self.module
		will need to call this; we'll only check for validity and they must
		check if they even want to run this.
		We'll also apply points/streak, though.'''
		# Hey, smartass, don't answer before you know the question!
		if self.started == None: 
			return

		now = datetime.now()
		tdiff = now - self.started
		tdelta = tdiff.seconds + tdiff.microseconds/1000000.
		# No human can answer in one second or less (compat with orig Trivia)
		if tdelta <= 1:
			return

		found = False
		for a in self.answer:
			try:
				# Issue #10: do not accept '200' when the correct answer is '20'
				float(a)
				if answer == a:
					found = True
					break
			except ValueError:
				try:
					if a.lower() in answer.lower():
						found = True
						break
				except:
					pass # XXX unicode

		if not found:
			return

		if self.winner == nick:
			self.streak += 1
		else:
			self.winner = nick
			self.streak = 1

		if self.hints_given == 0:
			points = 5
		else:
			points = 4 - self.hints_given

		points *= self.streak

		# XXX: We're COMPLETELY ignoring RFC1459 casemapping here
		cid = self.module.get_cid(self.cname)
		try:
			self.dbp.execute('SELECT points, fastest_time, highest_streak FROM trivia_scores WHERE nick = %s AND channel = %s',
					(nick, cid))
		except Exception, ex:
			self.module.elog.error('Unable to look up trivia scores: %s' % ex)
			return
		res = self.dbp.fetchone()
		totalpoints = 0
		fastest_time = 61 # greater than all of the timers added
		highest_streak = 0
		if res is not None:
			(totalpoints, fastest_time, highest_streak) = res

		totalpoints += points

		self.module.msg(self.cname, "Winner: %s; Answer: %s; Time: %.3fs; Streak: %d; Points: %d; Total: %d"
				% (nick, self.answer[0], tdelta, self.streak, points, totalpoints))

		if fastest_time > tdelta:
			fastest_time = tdelta
		if self.streak > highest_streak:
			highest_streak = self.streak

		if res is not None:
			self.dbp.execute('UPDATE trivia_scores SET points = %s, fastest_time = %s, highest_streak = %s WHERE nick = %s AND channel = %s',
					(totalpoints, fastest_time, highest_streak, nick.lower(), cid))
		else:
			self.dbp.execute('INSERT INTO trivia_scores(points, fastest_time, highest_streak, nick, channel) VALUES (%s, %s, %s, %s, %s)',
					(totalpoints, fastest_time, highest_streak, nick.lower(), cid))

		self.question_timer.stop()
		self.hint_timer.stop()
		self.answer = []
		self.hints_given = 0
		self.hint_timer = task.LoopingCall(self.ask)
		self.hint_timer.start(5, False)

	def stop(self, forced):
		'''Stops a round of trivia. This stops all timers associated with this
		instance and sends a message to the channel that the round is done.
		If forced is True, the message "Trivia stopped." will be shown,
		otherwise the message will be "Round of trivia complete."
		The calling instance is responsible of removing any references to us,
		this includes the dict entry.'''
		msg = ""

		try:
			self.question_timer.stop()
			self.hint_timer.stop()
		except AttributeError:
			pass # We might be called before hint_timer's set up

		if forced:
			msg = "Trivia stopped."
		else:
			msg = "Round of trivia complete."
		self.module.msg(self.cname,
				msg + " '.trivia [number]' to start playing again.")

