#!/usr/bin/python pseudoserver.py
# psm_trvia.py
# 
# based on psm_quotes.py, which is based on psm_limitserv.py written by
# celestin - martin <martin@rizon.net>

import sys
import types
import datetime
import random
from istring import istring
from pseudoclient import sys_log, sys_options, sys_channels, inviteable
from utils import *

from pyva import *
import logging
from core import *
from plugin import *

import cmd_admin, sys_auth, trivia_engine

import pyva_net_rizon_acid_core_Acidictive as Acidictive
import pyva_net_rizon_acid_core_AcidCore as AcidCore
import pyva_net_rizon_acid_core_User as User

class trivia(
	AcidPlugin,
	inviteable.InviteablePseudoclient
):
	initialized = False

	# (table name, display name); display name is currently forced to be
	# compatible with original Trivia
	themes = (('anime', 'Anime'),
			  ('default', 'default'),
			  ('geography', 'Geography'),
			  ('history', 'History'), 
			  ('lotr-books', 'LOTR-Books'),
			  ('lotr-movies', 'LOTR-Movies'),
			  ('movies', 'Movies'),
			  ('naruto', 'Naruto'),
			  ('sciandnature', 'ScienceAndNature'),
			  ('simpsons', 'Simpsons'),
			  ('sg1qs', 'Stargate'))

	# channel name => Trivia instance
	trivias = {}
	
	def start_threads(self):
		self.options.start()
		self.channels.start()
		self.auth.start()
		
	def bind_function(self, function):
		func = types.MethodType(function, self, trivia)
		setattr(trivia, function.__name__, func)
		return func

	def bind_admin_commands(self):
		list = cmd_admin.get_commands()
		self.commands_admin = []

		for command in list:
			self.commands_admin.append((command, {'permission': 'j', 'callback': self.bind_function(list[command][0]), 
												'usage': list[command][1]}))

	def __init__(self):
		AcidPlugin.__init__(self)

		self.name = "trivia"
		self.log = logging.getLogger(__name__)

		try:
			self.nick = istring(self.config.get('trivia').get('nick'))
		except Exception, err:
			self.log.exception("Error reading 'trivia:nick' configuration option: %s" % err)
			raise

		try:
			self.chan = istring(self.config.get('trivia').get('channel'))
		except Exception, err:
			self.log.exception("Error reading 'trivia:channel' configuration option: %s" % err)
			raise

		try:
			self.maxrounds = int(self.config.get('trivia').get('maxrounds'))
		except Exception, err:
			self.log.exception("Error reading 'trivia:maxrounds' configuration option: %s" % err)
			raise
		
		self.bind_admin_commands()
					
	def start(self):
		try:
			self.dbp.execute("CREATE TABLE IF NOT EXISTS trivia_chans (id INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT, name VARCHAR(200) NOT NULL, theme VARCHAR(64), UNIQUE KEY(name)) ENGINE=MyISAM")
			self.dbp.execute("CREATE TABLE IF NOT EXISTS trivia_scores (nick VARCHAR(30) DEFAULT NULL, points INT(10) DEFAULT '0', fastest_time FLOAT DEFAULT NULL, highest_streak INT(10) DEFAULT NULL, channel INT(10) DEFAULT NULL, KEY `idx` (`channel`,`points`) USING BTREE) ENGINE=MyISAM DEFAULT CHARSET=latin1;")
		except Exception, err:
			self.log.exception("Error creating table for trivia module (%s)" % err)
			raise
		
		try:
			AcidPlugin.start(self)
			inviteable.InviteablePseudoclient.start(self)

			self.options = sys_options.OptionManager(self)
			self.elog = sys_log.LogManager(self)
			self.auth = sys_auth.TriviaAuthManager(self)
			self.channels = sys_channels.ChannelManager(self)
		except Exception, err:
			self.log.exception('Error initializing subsystems for trivia module (%s)' % err)
			raise

		for channel in self.channels.list_valid():
			self.join(channel.name)

		self.elog.debug('Joined channels.')
		
		try:
			self.start_threads()
		except Exception, err:
			self.log.exception('Error starting threads for trivia module (%s)' % err)
			raise

		self.initialized = True
		self.elog.debug('Started threads.')
		return True
		
	def stop(self):
		if hasattr(self, 'auth'):
			self.auth.stop()

		if hasattr(self, 'channels'):
			if self.initialized:
				self.channels.force()

			self.channels.stop()
			self.channels.db_close()

		if hasattr(self, 'options'):
			if self.initialized:
				self.options.force()

			self.options.stop()
			self.options.db_close()

		for cname, trivia in self.trivias.iteritems():
			trivia.stop(True)

		self.trivias.clear()

	def join(self, channel):
		super(trivia, self).join(channel)
		self.dbp.execute("INSERT IGNORE INTO trivia_chans(name) VALUES(%s)", (str(channel),))

	def part(self, channel):
		super(trivia, self).part(channel)

		self.stop_trivia(channel, True)
		self.dbp.execute("DELETE FROM trivia_chans WHERE name=%s", (str(channel),))

	def msg(self, target, message):
		if message != '':
			Acidictive.privmsg(self.nick, target, format_ascii_irc(message))

	def multimsg(self, target, count, intro, separator, pieces, outro = ''):
		cur = 0

		while cur < len(pieces):
			self.msg(target, intro + separator.join(pieces[cur:cur + count]) + outro)
			cur += count
		
	def notice(self, target, message):
		if message != '':
			Acidictive.notice(self.nick, target, format_ascii_irc(message))
			
## Begin event hooks
	
	def get_cid(self, cname):
		"""Fetches the channel id for a given channel name."""
		self.dbp.execute("SELECT id FROM trivia_chans WHERE name = %s", (cname,))
		cid = self.dbp.fetchone()[0]
		return cid

	def stop_trivia(self, cname, forced):
		'''Stops a trivia instance and removes it from our dict.'''
		if istring(cname) not in self.trivias:
			return

		self.trivias[istring(cname)].stop(forced)
		del self.trivias[istring(cname)]

	def onPrivmsg(self, source, target, message):
	# Parse ADD/DEL requests
		if not self.initialized:
			return

		# HACKY
		# if inviteable didn't catch the command, it means we can handle it here instead
		if not inviteable.InviteablePseudoclient.onPrivmsg(self, source, target, message):
			return

		myself = User.findUser(self.nick)
		channel = target

		userinfo = User.findUser(source)
		sender = userinfo['nick']

		msg = message.strip()
		index = msg.find(' ')

		if index == -1:
			command = msg
			arg = ''
		else:
			command = msg[:index]
			arg = msg[index + 1:]
		
		command = command.lower()

		if self.channels.is_valid(channel): # a channel message
			if command.startswith("."): # a command
				# Log each and every command. This is very, very abusive and
				# thus should NOT be used in production if it can be helped.
				# But who cares about ethics? Well, I apparently don't.
				self.elog.debug("%s:%s > %s" % (sender, channel, msg[1:]))
				command = command[1:]
				if command == 'help' and arg == '':
					self.notice(sender, "Trivia: .help trivia - for trivia commands.")
				elif command == 'help' and arg.startswith('trivia'):
					self.notice(sender, "Trivia: .trivia [number] - to start playing. (max {0} rounds)".format(self.maxrounds))
					self.notice(sender, "Trivia: .strivia - to stop current round.")
					self.notice(sender, "Trivia: .topten/.tt - lists top ten players.")
					self.notice(sender, "Trivia: .rank [nick] - shows yours or given nicks current rank.")
					# Defunct in orig trivia
					#self.notice(sender, "Trivia: .next - skips question.")
					self.notice(sender, "Trivia: .themes - lists available question themes.")
					self.notice(sender, "Trivia: .theme set <name> - changes current question theme (must be channel founder).")
				elif command == 'trivia':
					if istring(channel) in self.trivias:
						self.elog.debug("Trivia, but we're in %s" % channel)
						return

					rounds = self.maxrounds
					if arg.isdigit() and int(arg) > 0:
						rounds = min(int(arg), self.maxrounds)

					self.dbp.execute("SELECT theme FROM trivia_chans WHERE name = %s", (channel,))
					theme = self.dbp.fetchone()[0]
					if not theme:
						theme = "default"

					self.trivias[istring(channel)] = trivia_engine.Trivia(channel, self, theme, int(rounds))
				elif command == 'strivia':
					# stop_trivia does sanity checking
					self.stop_trivia(channel, True)
				elif command == 'topten' or command == 'tt':
					self.dbp.execute("SELECT nick, points FROM trivia_scores WHERE channel = %s ORDER BY points DESC LIMIT 10", (self.get_cid(channel),))
					# XXX: This is silent if no entries have been done; but
					# original trivia does so, too. Silly behavior?
					out = []
					for i, row in enumerate(self.dbp.fetchall()):
						# i+1 = 1 should equal out[0]
						out.append("%d. %s %d" % (i+1, row[0], row[1]))
						
					if len(out) == 0:						
						self.notice(sender, "No one has played yet.")
					else:
						self.notice(sender, '; '.join(out))
				elif command == 'rank':
					if arg != '':
						querynick = arg.lower()
					else:
						querynick = sender.lower()
					self.dbp.execute("SELECT nick, points FROM trivia_scores WHERE channel = %s ORDER BY points DESC;", (self.get_cid(channel),))
					rows = self.dbp.fetchall()
					for i in range(len(rows)):
						userdata = rows[i]
						if userdata[0].lower() == querynick.lower():
							out = "%s is currently ranked #%d with %d %s" % (userdata[0], i + 1, userdata[1], "point" if userdata[1] == 1 else "points")
							if i > 0:
								otherdata = rows[i - 1]
								diff = otherdata[1] - userdata[1]
								out += ", %d %s behind %s" % (diff, "point" if diff == 1 else "points", otherdata[0])
							out += "."
							self.notice(sender, out)
							break
					else:
						self.notice(sender, "Nickname not found.")
				#elif command == 'next': # Defunct in orig trivia
					#pass
				elif command == 'themes':
					for theme in self.themes:
						query = "SELECT COUNT(tq.id) FROM `trivia_questions` AS `tq` JOIN `trivia_themes` AS `tt` ON tq.theme_id=tt.theme_id AND tt.theme_name='%s'"
						self.dbp.execute(query % theme[1])
						self.notice(sender, "Theme: %s, %d questions" %
								(theme[1], self.dbp.fetchone()[0]))
				elif command == 'theme':
					args = arg.split(' ')
					settheme = ""
					if len(args) < 2 or args[0].lower() != 'set':
						return

					self.notice(sender, "Checking if you are the channel founder.")
					self.auth.request(sender, channel, 'set_theme_' + args[1])
			else: # not a command, but might be an answer!
				if istring(channel) not in self.trivias:
					return # no trivia running, disregard that

				self.trivias[istring(channel)].check_answer(sender, msg)
	
	def onChanModes(self, prefix, channel, modes):
		if not self.initialized:
			return

		if not modes == '-z':
			return

		if channel in self.channels:
			self.channels.remove(channel)
			try:
				self.dbp.execute("DELETE FROM trivia_scores WHERE channel = %s", (self.get_cid(channel),))
			except:
				pass
			self.dbp.execute("DELETE FROM trivia_chans WHERE name = %s", (channel,))
			self.elog.request('Channel @b%s@b was dropped. Deleting it.' % channel)
	
	def getCommands(self):
		return self.commands_admin
