from urllib import urlencode
from feed import XmlFeed

class Bing(object):
	def __init__(self, key):
		self.API_KEY = key
		self.namespaces = {
 			'array': 'http://schemas.microsoft.com/2003/10/Serialization/Arrays',
 			'e': 'http://schemas.microsoft.com/2003/10/Serialization/',
		}
		f = XmlFeed('http://api.microsofttranslator.com/V2/Http.svc/GetLanguagesForTranslate?appId=' + self.API_KEY, namespaces=self.namespaces)
		self.languages = [lang.text.lower() for lang in f.elements('/array:ArrayOfstring')[0].children()]
		
	def translate(self, text, source=None, target='en'):
		url = 'http://api.microsofttranslator.com/V2/Http.svc/Translate?'
		url += urlencode({'text': text,
			'to': target,
			'appId': self.API_KEY})
		if source:
			url += '&from=' + source
		
		xml = XmlFeed(url, self.namespaces)
		return xml.text('/e:string')
	
	def detect_language(self, text):
		url = 'http://api.microsofttranslator.com/V2/Http.svc/Detect?'
		url += urlencode({'text': text,
			'appId': self.API_KEY})

		xml = XmlFeed(url, self.namespaces)
		return xml.text('/e:string')
