# Steam API for Internets bot
from feed import get_json
import requests
from BeautifulSoup import BeautifulSoup
from BeautifulSoup import BeautifulStoneSoup as XMLParser

class Steam(object):	
	def __init__(self, key):
		"""Initialises Steam object, sets API key."""
		self.api_key = key

	def find_user(self, steamid):
		steam_data = {}
		urlCommunity = 'http://steamcommunity.com/id/%s?xml=1' % steamid		
		responseXML = XMLParser(requests.get(urlCommunity).text)
		
		if responseXML.response != None:			
			return self.get_status(steamid)
		else:
			steam_data['steamid'] = responseXML.steamid64.text
			steam_data['personaname'] = responseXML.steamid.text
			return steam_data

	def get_status(self, steamid):
		"""Gets data from the steam website, returns JSON object with data."""
		url = 'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=%s&steamids=%s&format=json' % (self.api_key, steamid)
		response = get_json(url)['response']['players']
		if len(response) == 0:
			raise SteamException('No user found')
		return response[0]

	def get_games(self, userid):
		"""Gets list of games owned from the server, assumes userid is correct."""
		url = 'http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=%s&steamid=%s&include_played_free_games=1&format=json' % (self.api_key, userid)
		response = get_json(url)['response']
		return response

	def get_game_name(self, gameid):
		"""Gets game info from the server, assumes gameid is correct."""
		url = 'http://steamcommunity.com/app/%s' % gameid
		response = requests.get(url)		
		soup = BeautifulSoup(response.text)
		title = soup.title.string.replace("Steam Community :: ", "")		
		return title

class SteamException(Exception):
	"""Steam Exception, raised when something goes wrong when interacting with the API."""
	def __init__(self, msg):
		self.msg = msg

	def __str__(self):
		return str(self.msg)

class SteamUser(object):
	"""Steam User, contains a mapping of nick and steamid."""
	__slots__ = ['nick', 'steamid']
	def __init__(self, nick, steamid):
		self.nick = nick
		self.steamid = steamid
