import json
import re
from datetime import datetime
from urllib import urlencode
from feed import HtmlFeed, XmlFeed, get_json
from utils import unescape

RE_YOUTUBE_DURATION = re.compile(r'P((?:\d)+D)?T?(\d+H)?(\d+M)?(\d+S)?')


class Google(object):
	def __init__(self, api_key, yt_delay):
		self.api_key = api_key
		self.yt_delay = yt_delay
		self.yt_cache = {}    # {videoId: {'ts': datetime-timestamp, 'payload': data},... }
		self.yt_chanvids = {} # {channel: {videoId: datetime-timestamp, ...},... }

	def _check_link_eligibility(self, channel, videoId):
		if channel not in self.yt_chanvids:
			self.yt_chanvids[channel] = { videoId: datetime.now() }
		elif videoId not in self.yt_chanvids[channel]:
			self.yt_chanvids[channel][videoId] = datetime.now()
		else:
			if (datetime.now() - self.yt_chanvids[channel][videoId]).total_seconds() < self.yt_delay:
				return False
			else:
				self.yt_chanvids[channel][videoId] = datetime.now()
		return True

	def search(self, query, userip=None):
		url = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&'
		parameters = {'q': query}
		if userip:
			parameters['userip'] = userip
		url += urlencode(parameters)
		json = get_json(url)
		return json
	
	def image_search(self, query, userip=None):
		url = 'http://ajax.googleapis.com/ajax/services/search/images?v=1.0&'
		parameters = {'q': query}
		if userip:
			parameters['userip'] = userip
		url += urlencode(parameters)
		json = get_json(url)
		return json

	def yt_search(self, query, num=0, userip=None):
		url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&order=relevance&type=video&maxResults=5&'
		parameters = {'q': query, 'key': self.api_key}
		if userip:
			parameters['userip'] = userip
		
		url += urlencode(parameters)
		js = get_json(url)
		if not js['items']:
			return None
		video_info = js['items'][num]

		return self.yt_video(video_info['id']['videoId'], userip)

	def yt_video(self, video, userip=None):
		for entry in self.yt_cache.keys():
			if (datetime.now() - self.yt_cache[entry]['ts']).total_seconds() > 300: # Cache data for 5 minutes
				del self.yt_cache[entry]
			elif entry == video:
				return self.yt_cache[video]['payload']

		url = 'https://www.googleapis.com/youtube/v3/videos?&part=snippet,contentDetails,statistics&'
		parameters = {'id': video, 'key': self.api_key}
		if userip:
			parameters['userip'] = userip
		
		url += urlencode(parameters)
		js = get_json(url)
		video_info = js['items'][0]
		m = RE_YOUTUBE_DURATION.search(video_info['contentDetails']['duration'])
		days, hours, minutes, seconds = map(lambda x: int(x[:-1]) if x else 0, m.groups())
		stats = video_info['statistics']
		
		data = {
			'title': video_info['snippet']['title'],
			'duration': 86400 * days + 3600 * hours + 60 * minutes + seconds,
			'uploaded': video_info['snippet']['publishedAt'],
			'id': video_info['id'],
			'favorite_count': int(stats['favoriteCount']),
			'view_count': int(stats['viewCount']),
			'liked': int(stats['likeCount']) if 'likeCount' in stats else 0,
			'disliked': int(stats['dislikeCount']) if 'dislikeCount' in stats else 0
			}
		self.yt_cache[video] = {'ts': datetime.now(), 'payload': data}
		return data
