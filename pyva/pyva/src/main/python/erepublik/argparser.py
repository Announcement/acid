from api import utils
from copy import copy
from pseudoclient.argparser import *


def check_rank(option, opt, value):
	try:
		return utils.get_rank(value)
	except:
		raise OptionValueError('option %s: invalid rank: %s' % (opt, value))

def check_quality(option, opt, value):
	try:
		return utils.get_quality(value)
	except:
		raise OptionValueError('option %s: invalid quality: %s' % (opt, value))

def check_industry(option, opt, value):
	try:
		return utils.get_industry(value)
	except:
		raise OptionValueError('option %s: invalid industry: %s' % (opt, value))

def check_domain(option, opt, value):
	try:
		return utils.get_domain(value)
	except:
		raise OptionValueError('option %s: invalid domain: %s' % (opt, value))


class ErepublikParserOption(ArgumentParserOption):
	TYPES = ArgumentParserOption.TYPES + ('rank', 'quality', 'wellness', 'happiness', 'industry', 'domain')
	TYPE_CHECKER = copy(ArgumentParserOption.TYPE_CHECKER)
	TYPE_CHECKER['rank'] = check_rank
	TYPE_CHECKER['quality'] = check_quality
	TYPE_CHECKER['wellness'] = check_positive_decimal
	TYPE_CHECKER['happiness'] = check_positive_decimal
	TYPE_CHECKER['industry'] = check_industry
	TYPE_CHECKER['domain'] = check_domain
