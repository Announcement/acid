from copy import copy
from decimal import Decimal, InvalidOperation
from optparse import *


def check_positive_decimal(option, opt, value):
	try:
		d = Decimal(value)
	except InvalidOperation:
		raise OptionValueError('option %s: invalid decimal value: %s' % (opt, value))

	if d >= 0 and d <= 1000000:
		return d
	else:
		raise OptionValueError('option %s: expected value (0 <= x <= 1000000), got %s instead' % (opt, d))

def check_positive_integer(option, opt, value):
	try:
		i = int(value)
	except ValueError:
		raise OptionValueError('option %s: invalid integer value: %s' % (opt, value))

	if i >= 0:
		return i
	else:
		raise OptionValueError('option %s: expected positive value, got %d instead' % (opt, i))


class ArgumentParserOption(Option):
	TYPES = Option.TYPES + ('+decimal', '+integer')
	TYPE_CHECKER = copy(Option.TYPE_CHECKER)
	TYPE_CHECKER['+decimal'] = check_positive_decimal
	TYPE_CHECKER['+integer'] = check_positive_integer

class ArgumentParserError(Exception):
	def __init__(self, message):
		self.msg = message

	def __str__(self):
		return str(self.msg)

class ArgumentParser(OptionParser):
	def error(self, error):
		raise ArgumentParserError(error)
