import json
import socket
import urllib2
#import lxml.html
from BaseHTTPServer import BaseHTTPRequestHandler
from decimal import Decimal
from StringIO import StringIO
from urlparse import urlparse

class InputError(Exception):
	def __init__(self, msg):
		self.msg = msg

	def __str__(self):
		return str(self.msg)

class FeedError(Exception):
	def __init__(self, e):
		if hasattr(e, 'code'):
			c = e.code

			if c == 404:
				self.msg = 'not found.'
			elif c == 406:
				self.msg = 'this e-Sim API feed is unavailable.'
			elif c == 500:
				self.msg = 'e-Sim server has encountered an unexpected error.'
			elif c == 502:
				self.msg = 'invalid response from e-Sim server. Try again later.'
			elif c == 503:
				self.msg = 'e-Sim API feed is temporarily unavailable. Try again later.'
			else:
				self.msg = 'something went wrong while connecting to e-Sim API feed (%s)' % BaseHTTPRequestHandler.responses[e.code][0]

			self.code = c
			self.url = e.url
		elif hasattr(e, 'reason'):
			r = str(e.reason)

			if r == 'timed out':
				self.msg = 'connection to e-Sim API feed timed out. Try again later.'
			else:
				self.msg = r

			self.code = None
			self.url = None
		else:
			e = unicode(e)
			
			if e == 'No citizen with such name':
				self.msg = 'Requested citizen was not found.'
			elif e == "No JSON object could be decoded":
				self.msg = 'e-Sim API feed is temporarily unavailable. Try again later.'
			else:
				self.msg = e
			
			self.code = None
			self.url = None
	
	def __str__(self):
		return self.msg

class HtmlFeed:
	def __init__(self, value):
		if value == None:
			raise InputError('Invalid feed input.')

		if isinstance(value, str) or isinstance(value, unicode):
			try:
				opener = urllib2.build_opener()
				opener.addheaders = [('User-Agent', 'Rizon e-Sim bot - www.rizon.net')]
				feed = opener.open(value.replace(' ', '%20'), timeout=20)
				self._html = feed.read()
				feed.close()
			except urllib2.URLError, e:
				raise FeedError(e)
		else:
			raise InputError('Invalid feed input type.')

	def html(self):
		return self._html

def get_json(value):
	if value == None:
		raise InputError('Invalid feed input.')
	
	if isinstance(value, basestring):
		feed = HtmlFeed(value)
		try:
			return json.load(StringIO(feed.html()))
		except ValueError, r:
			raise FeedError(r)
	else:
		raise InputError('Invalid feed input type.')
