package net.rizon.acid.plugins.trapbot;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Message;
import net.rizon.acid.core.Protocol;

class RetrapTimer implements Runnable
{
	/*
	 * release users
	 */
	@Override
	public void run()
	{
		/* start enforcing again and announce it */
		trapbot.enforce = true;
		trapbot.retrapTimer = null;

		Protocol.privmsg(trapbot.trapbot.getUID(), trapbot.getTrapChanName(), Message.BOLD + "HAHA, TRAPPED AGAIN!" + Message.BOLD);

		// magic here, too
		trapbot.releaseTimer = Acidictive.schedule(new ReleaseTimer(), new Random().nextInt(2700) + 2700, TimeUnit.SECONDS);
	}
}
