package net.rizon.acid.plugins.trapbot.commands;

import net.rizon.acid.core.AcidCore;
import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.core.User;
import net.rizon.acid.plugins.trapbot.trapbot;

public class Untrap extends Command
{
	public Untrap()
	{
		super(1, 1);
	}

	@Override
	public void Run(User x, AcidUser to, Channel c, final String[] args)
	{
		User u = User.findUser(args[0]);

		if (u == null || u.getServer() == AcidCore.me || u.getServer().isUlined())
		{
			Acidictive.reply(x, to, c, "Cannot find user \2" + args[0] + "\2.");
			return;
		}

		Channel trapchan = Channel.findChannel(trapbot.getTrapChanName());
		if (trapchan == null)
		{
			Acidictive.reply(x, to, c, "Cannot find trapchan");
			return;
		}

		if (!u.isOnChan(trapchan))
		{
			Acidictive.reply(x, to, c, "User " + u.getNick() + " isn't on " + trapbot.getTrapChanName());
			return;
		}

		if (u.hasMode("o"))
		{
			Acidictive.reply(x, to, c, "Access denied.");
			return;
		}

		trapbot.freeUser(u);

		String reason = "You're free now!";

		Acidictive.kick(trapbot.trapbot, u, trapchan, reason);

		Acidictive.reply(x, to, c, "User \2" + u.getNick() + "\2 was freed and kicked.");
	}

	@Override
	public boolean onHelpCommand(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "Syntax: \2untrap [user]\2");
		Acidictive.reply(u, to, c, " ");
		Acidictive.reply(u, to, c, "Frees and kicks a given user from the trap channel.");

		return true;
	}
}
