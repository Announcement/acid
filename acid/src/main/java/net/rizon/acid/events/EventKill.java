package net.rizon.acid.events;

import net.rizon.acid.core.User;

public class EventKill
{
	private String killer;
	private User user;
	private String reason;

	public String getKiller()
	{
		return killer;
	}

	public void setKiller(String killer)
	{
		this.killer = killer;
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public String getReason()
	{
		return reason;
	}

	public void setReason(String reason)
	{
		this.reason = reason;
	}
}
