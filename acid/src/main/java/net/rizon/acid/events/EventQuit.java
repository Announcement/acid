package net.rizon.acid.events;

import net.rizon.acid.core.User;

public class EventQuit
{
	private User user;
	private String msg;
	private boolean quitStorm = false;

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	/**
	 * Sets whether or not this event is caused by a quit storm.
	 *
	 * @param isQuitStorm True if event is caused by quit storm, false
	 *                    otherwise.
	 */
	public void setQuitStorm(boolean isQuitStorm)
	{
		this.quitStorm = isQuitStorm;
	}

	/**
	 * Gets if this event is caused by the Capability QuitStorm.
	 *
	 * @return True if part of a QuitStorm, false otherwise.
	 */
	public boolean isQuitStorm()
	{
		return this.quitStorm;
	}

	public String getMsg()
	{
		return msg;
	}

	public void setMsg(String msg)
	{
		this.msg = msg;
	}
}
