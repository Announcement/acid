package net.rizon.acid.core;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import net.rizon.acid.conf.AccessPreset;
import net.rizon.acid.events.EventUserMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class User implements Comparable<User>
{
	private static final Logger log = LoggerFactory.getLogger(User.class);

	private String nick, user, host, vhost, name, identnick, modes, UID, ip,
			certfp, authflags, su;
	private String cloakedIp, cloakedHost;
	private int nickTS, conTS;
	private Server server;
	private String flags; // Access flags
	private ArrayList<String> oldNicks;
	private Hashtable<String, ArrayList<Integer>> chans;
	private HashSet<Channel> chanList;

	public User(String nick, String user, String host, String vhost, String name,
			Server server, int nickTS, int conTS, String modes, String UID, String ip)
	{
		this.nick = nick;
		this.user = user;
		this.host = host;
		this.vhost = vhost;
		this.host = host;
		this.name = name;
		this.server = server;
		this.identnick = "";
		this.nickTS = nickTS;
		this.conTS = conTS;
		this.modes = modes;
		this.UID = UID;
		this.ip = ip;
		// Check if this IP isn't spoofed.
		if (!this.ip.equals("255.255.255.255") && !this.ip.equals("0"))
		{
			// Cloak IP if IP is visible host, else cloak hostname.
			this.cloakedIp = Acidictive.cloakGenerator.cloak(this.ip);
			if (this.ip.equals(this.host))
			{
				this.cloakedHost = this.cloakedIp;
			}
			else
			{
				this.cloakedHost = Acidictive.cloakGenerator.cloak(this.host);
			}
		}
		else
		{
			this.cloakedIp = this.ip;
			this.cloakedHost = this.host;
		}
		this.flags = "";
		this.certfp = "";
		this.authflags = "";
		this.su = "";
		oldNicks = new ArrayList<String>();
		chans = new Hashtable<String, ArrayList<Integer>>();
		chanList = new HashSet<Channel>();

		uidMap.put(UID, this);
		nickMap.put(nick.toLowerCase(), this);

		this.getServer().incUsers();
	}

	public void onQuit()
	{
		for (Iterator<Channel> it = chanList.iterator(); it.hasNext();)
		{
			Channel chan = it.next();
			chan.removeUser(this);
			it.remove();
		}

		this.getServer().decUsers();
		if (!(this instanceof AcidUser))
			UserList.decreaseHost(this.ip);

		uidMap.remove(UID);
		nickMap.remove(nick.toLowerCase());
	}

	public void flush()
	{
		oldNicks.clear();
		chans.clear();
	}

	public void clearFlood()
	{
		if (chans != null)
			chans.clear();
	}

	public ArrayList<Integer> addChanJoinPart(String chan, int TS)
	{
		ArrayList<Integer> x = chans.get(chan.toLowerCase());
		if (x == null)
		{
			ArrayList<Integer> tmp = new ArrayList<Integer>();
			tmp.add(new Integer(TS));
			chans.put(chan.toLowerCase(), tmp);
			return tmp;
		}
		else
		{
			x.add(new Integer(TS));
			if (x.size() > 4)
				x.remove(0);
			return x;
		}

	}

	public boolean isOnChan(Channel chan)
	{
		return chanList.contains(chan);
	}

	public void addChan(Channel chan)
	{
		chanList.add(chan);
	}

	public void remChan(Channel chan)
	{
		chanList.remove(chan);
	}

	public Set<Channel> getChannels()
	{
		return this.chanList;
	}

	public String getChanStr()
	{
		StringBuilder sb = new StringBuilder();

		for (Channel c : chanList)
		{
			if (sb.length() > 0)
				sb.append(' ');

			if (c.hasMode('s'))
				sb.append('!');
			
			sb.append(c.getName());
		}

		if (sb.length() == 0)
			return "none";

		return sb.toString();
	}

	public void setRealhost(String ip, String host)
	{
		this.ip = ip;
		this.host = host;
	}

	public void setFlags(String flags)
	{
		this.flags = flags;
	}

	public void setUID(String uid)
	{
		this.UID = uid;
	}

	public void changeNick(String newNick)
	{
		String oldnick = this.getNick();

		nickMap.remove(this.getNick().toLowerCase());
		oldNicks.add(this.getNick());
		if (oldNicks.size() > 20)
			oldNicks.remove(0);
		this.nick = newNick;
		nickMap.put(this.getNick().toLowerCase(), this);

		this.setMode("-r");

		for (Channel chan : chanList)
		{
			chan.onNick(oldnick, this.getNick());
		}
	}

	// accessor methods
	public String getIdentNick()
	{
		return identnick;
	}

	public String getIP()
	{
		return ip;
	}

	public String getNick()
	{
		return nick;
	}

	public boolean hasMode(String mode)
	{
		return modes.contains(mode);
	}

	public void setMode(String mode)
	{
		if (mode.equals("") || mode == null)
			return;

		String old = modes;

		boolean plus = true;
		String chr;
		for (int x = 0; x < mode.length(); x++)
		{
			chr = mode.substring(x, x + 1);
			if (chr.equals("+"))
			{
				plus = true;
			}
			else if (chr.equals("-"))
			{
				plus = false;
			}
			else if (plus && modes.indexOf(chr) == -1)
			{
				modes += chr;
			}
			else if (!plus && modes.indexOf(chr) >= 0)
			{
				if (chr.equals("x"))
					this.vhost = this.host;
				modes = modes.replaceAll(chr, "");
			}
		}

		EventUserMode event = new EventUserMode();
		event.setNewmodes(modes);
		event.setOldmodes(old);
		event.setUser(this);
		Acidictive.eventBus.post(event);
	}

	public String getUser()
	{
		return user;
	}

	public String getUID()
	{
		return UID;
	}

	public String getHost()
	{
		return host;
	}

	public String getVhost()
	{
		return vhost;
	}

	public void setVhost(String vhost)
	{
		this.vhost = vhost;
	}

	public String getRealName()
	{
		return name;
	}

	public int getNickTS()
	{
		return nickTS;
	}

	public int getConTS()
	{
		return conTS;
	}

	public void changeNickTS(int nickTS)
	{
		this.nickTS = nickTS;
	}

	public Server getServer()
	{
		return server;
	}

	/* Does this user have all of these flags? Can also take a type (SRA/SA/SO/whatever) */
	public boolean hasFlags(String flags)
	{
		if (flags.isEmpty())
			return false;

		String my_flags = this.flags;

		for (AccessPreset p : Acidictive.conf.access_preset)
		{
			for (String s : p.name)
			{
				my_flags = my_flags.replace(s, p.privileges);
				flags = flags.replace(s, p.privileges);
			}
		}

		for (int i = 0; i < flags.length(); ++i)
			if (my_flags.indexOf(flags.charAt(i)) == -1)
				return false;

		return true;
	}
	
	public String getCloakedIp()
	{
		return this.cloakedIp;
	}
	
	public String getCloakedHost()
	{
		return this.cloakedHost;
	}

	public String getFlags()
	{
		return this.flags;
	}

	public String getModes()
	{
		return modes;
	}

	public String getCertFP()
	{
		return certfp;
	}

	public void setCertFP(String certfp)
	{
		this.certfp = certfp;
	}

	public String getAuthFlags()
	{
		return authflags;
	}

	public void setAuthFlags(String authflags)
	{
		this.authflags = authflags;
	}

	public String getSU()
	{
		return su;
	}

	public void setSU(String su)
	{
		this.su = su;
	}

	@Override
	public int compareTo(User user)
	{
		return nick.compareTo(user.getNick());
	}

	@Override
	public String toString()
	{
		return nick + "!" + user + "@" + host + "/" + ip + "/" + this.cloakedIp + "/" + this.cloakedHost + "/" + "(" + name + ")[" + server + "] / " + oldNicksList();
	}

	public String getNickString()
	{
		return nick + "!" + user + "@" + host + "/" + ip + "/" + this.cloakedIp + "/" + this.cloakedHost + "/" + vhost + "(" + name + ")";
	}

	public String getSNString()
	{
		return nick + "!" + user + "@" + host;
	}

	public String getNString()
	{
		return nick + "!" + user + "@" + host + "/" + ip + "/" + this.cloakedIp + "/" + this.cloakedHost + "/" + vhost + "(" + name + ")(" + modes + ")[" + server + "]";
	}

	public String getNStringON()
	{
		return nick + "!" + user + "@" + host + "/" + ip + "/" + this.cloakedIp + "/" + this.cloakedHost + "/" + vhost + "(" + name + ")(" + modes + ")[" + server + "] / " + oldNicksList();
	}

	public String getNickLine()
	{
		return getNick() + "!" + getUser() + "@" + getHost() + "/" + getRealName();
	}

	public ArrayList<String> oldNicks()
	{
		return oldNicks == null ? new ArrayList<String>() : oldNicks;
	}

	public String oldNicksList()
	{
		if (oldNicks == null)
			return "No nick changes.";
		if (oldNicks.size() > 0)
		{
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < oldNicks.size(); i++)
			{
				sb.append(oldNicks.get(i) + " -> ");
			}
			sb.append(nick);
			return sb.toString();
		}
		return "No nick changes.";
	}
	
	public boolean isIdentified()
	{
		if (this.hasMode("r"))
			return true;
		return this.su != null && !this.su.isEmpty() && !this.su.equals("*") && !Character.isDigit(this.su.charAt(0));
	}

	// TODO: Fixme to make this either properly static or a "real" method
	// XXX ??????????????????????????????????? wtf is this
	public void loadAccess(final User user)
	{
		try
		{
			PreparedStatement ps = Acidictive.acidcore_sql.prepare("SELECT `user`, `flags` FROM `access` WHERE `user` = ?");
			ps.setString(1, user.getSU());
			ResultSet rs = Acidictive.acidcore_sql.executeQuery(ps);
			if (rs.next())
			{
				String oldflags = this.flags;

				this.flags = rs.getString("flags");
				this.identnick = user.getSU();

				if (oldflags.equals(this.flags) == false)
					Acidictive.notice(this, "You have been logged in with flags " + this.flags);
			}
		}
		catch (SQLException ex)
		{
			log.warn("Unable to load access for user", ex);
		}
	}

	private static HashMap<String, User> uidMap = new HashMap<String, User>();
	private static HashMap<String, User> nickMap = new HashMap<String, User>();

	public static User findUser(final String nick)
	{
		if (nick != null && nick.isEmpty() == false && Character.isDigit(nick.charAt(0)))
			return uidMap.get(nick);
		return nickMap.get(nick.toLowerCase());
	}

	public static final Set<String> getUsers()
	{
		return nickMap.keySet();
	}

	public static Collection<User> getUsersC() { return nickMap.values(); }

	public static final String toName(final String uid)
	{
		User u = findUser(uid);
		if (u != null)
			return u.getNick();
		return uid;
	}

	public static int userCount()
	{
		return nickMap.size();
	}

	private static char[] currentUid = new char[] { 'A', 'A', 'A', 'A', 'A', '@' };

	public static String generateUID()
	{
		for (int i = 5; i >= 0; --i)
		{
			if (currentUid[i] == 'Z')
			{
				currentUid[i] = '0';
				break;
			}
			else if (currentUid[i] != '9')
			{
				currentUid[i]++;
				break;
			}
			else
				currentUid[i] = 'A';
		}

		return AcidCore.me.getSID() + new String(currentUid);
	}
}
