package net.rizon.acid.core;

import net.rizon.acid.io.IRCMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Protocol
{
	private static final Logger log = LoggerFactory.getLogger(Protocol.class);

	private static void send(String source, String command, Object... args)
	{
		IRCMessage message = new IRCMessage(source, command, args);
		AcidCore.send(message);
	}

	private static void privmsgInternal(final String sender, final String recipient, String message)
	{
		send(sender, "PRIVMSG", recipient, message);
	}

	private static void noticeInternal(final String sender, final String recipient, String message)
	{
		send(sender, "NOTICE", recipient, message);
	}

	private static void encap(final String server, final String command, Object... args)
	{
		Object[] o = new Object[args.length + 2];
		o[0] = server;
		o[1] = command;
		System.arraycopy(args, 0, o, 2, args.length);

		send(AcidCore.me.getSID(), "ENCAP", o);
	}

	public static void uplink(Server me, String password)
	{
		send(null, "PASS", password, "TS", "6", me.getSID());
		send(null, "CAPAB", "TS6 EX IE HUB KLN GLN ENCAP EOB TBURST QS");
		send(null, "SERVER", me.getName(), 1, me.getDescription());
		send(null, "SVINFO", 6, 6, 0, AcidCore.getTS());
	}

	public static void ping(String dest)
	{
		send(null, "PING", dest);
	}

	public static void pong(String dest)
	{
		send(null, "PONG", dest);
	}

	public static void privmsg(final String source, final String recipient, String msg)
	{
		String[] split = msg.split("\n");
		for (String s : split)
			do
			{
				int message_length = 450;
				boolean long_message = s.length() > message_length;
				String sub = s.substring(0, long_message ? message_length : s.length());
				privmsgInternal(source, recipient, sub);
				s = s.substring(long_message ? message_length : s.length());
			}
			while (s.isEmpty() == false);
	}

	public static void notice(final String source, final String recipient, String msg)
	{
		String[] split = msg.split("\n");
		for (String s : split)
			do
			{
				int message_length = 450;
				boolean long_message = s.length() > message_length;
				String sub = s.substring(0, long_message ? message_length : s.length());
				noticeInternal(source, recipient, sub);
				s = s.substring(long_message ? message_length : s.length());
			}
			while (s.isEmpty() == false);
	}

	public static void invite(AcidUser by, User u, Channel c)
	{
		send(by.getUID(), "INVITE", u.getUID(), c.getName(), c.getTS());
	}

	public static void join(User u, Channel c)
	{
		if (u.getServer() != AcidCore.me)
		{
			log.warn("Fake direction for JOIN " + u.getServer().getName() + " <-> " + AcidCore.me.getName());
			return;
		}

		join(u, "", c);
	}

	public static void join(User u, final String status, Channel c)
	{
		if (u.getServer() != AcidCore.me)
		{
			log.warn("Fake direction for SJOIN " + u.getServer().getName() + " <-> " + AcidCore.me.getName());
			return;
		}

		send(AcidCore.me.getSID(), "SJOIN", AcidCore.getTS(), c.getName(), "+" + c.getModes(true), status + u.getUID());
	}

	public static void part(User u, final String channel)
	{
		if (u.getServer() != AcidCore.me)
		{
			log.warn("Fake direction for PART " + u.getServer().getName() + " <-> " + AcidCore.me.getName());
			return;
		}

		send(u.getUID(), "PART", channel);
	}

	public static void quit(User u, final String reason)
	{
		if (u.getServer() != AcidCore.me)
		{
			log.warn("Fake direction for QUIT " + u.getServer().getName() + " <-> " + AcidCore.me.getName());
			return;
		}

		send(u.getUID(), "QUIT", reason);
	}

	public static void kick(User u, final String channel, final String reason)
	{
		send(AcidCore.me.getSID(), "KICK", channel, u.getUID(), reason);
	}

	public static void kick(AcidUser from, User u, final String channel, final String reason)
	{
		send(from.getUID(), "KICK", channel, u.getUID(), reason);
	}

	public static void kill(final String target, final String reason)
	{
		// Add kill path or the reason gets broken
		send(AcidCore.me.getSID(), "KILL", target, AcidCore.me.getName() + " (" + reason + ")");
	}

	public static void svskill(final String target)
	{
		encap("*", "SVSKILL", target);
	}

	public static void noop(Server s)
	{
		encap(s.getName(), "SVSNOOP", "+");
	}

	public static void unnoop(Server s)
	{
		encap(s.getName(), "SVSNOOP", "-");
	}

	public static void capture(User u)
	{
		send(AcidCore.me.getSID(), "CAPTURE", u.getUID());
	}

	public static void uncapture(User u)
	{
		send(AcidCore.me.getSID(), "UNCAPTURE", u.getUID());
	}

	public static void svsmode(User u, final String modes)
	{
		encap("*", "SVSMODE", u.getUID(), u.getNickTS(), modes);
	}

	public static void chgident(User u, final String host)
	{
		encap("*", "CHGIDENT", u.getUID(), host);
	}

	public static void chghost(User u, final String host)
	{
		encap("*", "CHGHOST", u.getUID(), host);
	}

	public static void svsnick(User u, final String newnick)
	{
		encap("*", "SVSNICK", u.getUID(), u.getNickTS(), newnick, AcidCore.getTS());
	}

	public static void svsjoin(User u, final String channel)
	{
		encap("*", "SVSJOIN", u.getUID(), channel);
	}

	public static void svspart(User u, final String channel)
	{
		encap("*", "SVSPART", u.getUID(), channel);
	}

	public static void resv(String source, Server s, final String what, final String reason)
	{
		send(source, "RESV", (s != null ? s.getName() : "*"), what, reason);
	}

	public static void unresv(String source, final String what)
	{
		send(source, "UNRESV", what);
	}

	public static void wallop(final String source, final String what)
	{
		send(source, "OPERWALL", what);
	}

	public static void kline(final String source, int time, final String user, final String host, final String reason)
	{
		if (host.replaceAll("[*.?]", "").isEmpty())
			return;

		send(source, "KLINE", "*", time, user, host, reason);
	}

	public static void unkline(final String source, final String user, final String host)
	{
		send(source, "UNKLINE", "*", user, host);
	}

	public static void mode(String source, String target, Object... args)
	{
		Object[] o = new Object[args.length + 1];
		o[0] = target;
		System.arraycopy(args, 0, o, 1, args.length);

		send(source, "MODE", o);
	}

	public static void uid(User user)
	{
		// :99h UID deso 1 1233151144 +Saiowy deso canuck 0 99hAAAAAB 0 canuck :canuck
		// :99h UID <nick> <hops> <ts> +<umodes> <user> <vhost> <ip> <uid> <svsts> <realhost> :<geco>
		send(AcidCore.me.getSID(), "UID", user.getNick(), 1, AcidCore.getTS(), "+" + user.getModes(), user.getUser(), user.getVhost(), 0, user.getUID(), 0, user.getHost(), user.getRealName());
	}

	public static void sid(Server server)
	{
		// :geo.rizon.net SID irc.test.net 2 98C :JUPED
		send(AcidCore.me.getSID(), "SID", server.getName(), (server.getHops() + 1), server.getSID(), server.getDescription());
	}

	public static void squit(Server server, final String reason)
	{
		send(AcidCore.me.getSID(), "SQUIT", server.getName(), reason);
	}

	public static void eob()
	{
		send(AcidCore.me.getSID(), "EOB");
	}

	public static void numeric(int num, String target, Object... args)
	{
		Object[] o = new Object[args.length + 1];
		o[0] = target;
		System.arraycopy(args, 0, o, 1, args.length);

		send(AcidCore.me.getSID(), "" + num, o);
	}

	public static void authflags(User target, String flags)
	{
		encap(target.getServer().getName(), "AUTHFLAGS", target.getUID(), flags);
	}

	public static void chgclass(User target, String clazz)
	{
		encap(target.getServer().getName(), "CHGCLASS", target.getUID(), clazz);
	}

	public static void chgrealhost(User target, String sockhost, String realhost)
	{
		encap(target.getServer().getName(), "CHGREALHOST", target.getUID(), sockhost, realhost);
	}

	public static void swebirc(String flag, String uid, String host, String ip, String password, String name, String fakeSockHost, String fakeHost)
	{
		if (flag.equalsIgnoreCase("REQ") || flag.equalsIgnoreCase("ACK") || flag.equalsIgnoreCase("NAK"))
		{
			encap("*", "SWEBIRC", flag.toUpperCase(), uid, host, ip, "*", "*", password, name, fakeSockHost, fakeHost);
		}
	}
}
