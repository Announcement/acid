package net.rizon.acid.commands;

import net.rizon.acid.core.AcidUser;
import net.rizon.acid.core.Acidictive;
import net.rizon.acid.core.Channel;
import net.rizon.acid.core.Command;
import net.rizon.acid.plugins.Plugin;
import net.rizon.acid.core.User;
import net.rizon.acid.plugins.PluginManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Plugins extends Command
{
	private static final Logger log = LoggerFactory.getLogger(Plugins.class);

	public Plugins()
	{
		super(1, 2);
	}

	@Override
	public void Run(User u, AcidUser to, Channel c, String[] args)
	{
		if (args[0].equalsIgnoreCase("LIST"))
		{
			int i = 0;
			for (Plugin p : PluginManager.getPlugins())
			{
				++i;
				Acidictive.reply(u, to, c, p.getName());
			}
			Acidictive.reply(u, to, c, i + " plugins loaded.");
		}
		else if (args[0].equalsIgnoreCase("LOAD") && args.length == 2)
		{
			String[] s = args[1].split(":");
			if (s.length != 3)
			{
				Acidictive.reply(u, to, c, "Format is groupId:artifactId:version");
				return;
			}

			Plugin p = PluginManager.findPlugin(s[1]);
			if (p != null)
			{
				Acidictive.reply(u, to, c, p.getName() + " is already loaded.");
				return;
			}

			try
			{
				p = PluginManager.loadPlugin(s[0], s[1], s[2]);
				Acidictive.reply(u, to, c, "Plugin " + p.getName() + " successfully loaded");
				log.info("PLUGINS LOAD for " + p.getName() + " from " + u.getNick());
			}
			catch (Exception ex)
			{
				log.warn("Unable to load plugin " + args[1], ex);
				Acidictive.reply(u, to, c, "Unable to load plugin " + args[1] + ": " + ex.getMessage());
			}
		}
		else if (args[0].equalsIgnoreCase("UNLOAD") && args.length == 2)
		{
			Plugin p = PluginManager.findPlugin(args[1]);
			if (p == null)
			{
				Acidictive.reply(u, to, c, args[1] + " is not loaded.");
				return;
			}

			if (p.isPermanent())
			{
				Acidictive.reply(u, to, c, p.getName() + " is permanent and can not be unloaded.");
				return;
			}

			PluginManager.unload(p);

			Acidictive.reply(u, to, c, "Unloaded plugin " + p.getName());
			log.info("PLUGINS UNLOAD for " + p.getName() + " from " + u.getNick());
		}
		else if (args[0].equalsIgnoreCase("RELOAD") && args.length == 2)
		{
			Plugin p = PluginManager.findPlugin(args[1]);
			if (p == null)
			{
				Acidictive.reply(u, to, c, args[1] + " is not loaded.");
				return;
			}

			if (p.isPermanent())
			{
				Acidictive.reply(u, to, c, p.getName() + " is permanent and can not be unloaded.");
				return;
			}

			PluginManager.unload(p);

			try
			{
				p = PluginManager.loadPlugin(p.getArtifact().getGroupId(), p.getArtifact().getArtifactId(), p.getArtifact().getVersion());
				Acidictive.reply(u, to, c, "Plugin " + p.getName() + " successfully reloaded");
				log.info("PLUGINS RELOAD for " + p.getName() + " from " + u.getNick());
			}
			catch (Exception ex)
			{
				log.warn("Unable to reload plugin " + args[1], ex);
				Acidictive.reply(u, to, c, "Unable to reload plugin " + args[1] + ": " + ex.getMessage());
			}
		}
		else
			Acidictive.reply(u, to, c, "Use LOAD, UNLOAD, RELOAD, or LIST");
	}

	@Override
	public void onHelp(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "\2plugins list\2 / Lists currently loaded plugins");
		Acidictive.reply(u, to, c, "\2plugins load <plugin>\2 / Loads a plugin");
		Acidictive.reply(u, to, c, "\2plugins reload <plugin>\2 / Reloads a plugin");
		Acidictive.reply(u, to, c, "\2plugins unload <plugin>\2 / Unloads a plugin");
	}

	@Override
	public boolean onHelpCommand(User u, AcidUser to, Channel c)
	{
		Acidictive.reply(u, to, c, "Syntax: \2shutdown\2");
		Acidictive.reply(u, to, c, "        \2plugins list\2 / Lists currently loaded plugins");
		Acidictive.reply(u, to, c, "        \2plugins load <plugin>\2 / Loads a plugin");
		Acidictive.reply(u, to, c, "        \2plugins reload <plugin>\2 / Reloads a plugin");
		Acidictive.reply(u, to, c, "        \2plugins unload <plugin>\2 / Unloads a plugin");
		Acidictive.reply(u, to, c, " ");
		Acidictive.reply(u, to, c, "The plugins command allows managing plugins. Note that plugins");
		Acidictive.reply(u, to, c, "may contain some lower unit of management (such as scripts in");
		Acidictive.reply(u, to, c, "the case of pyva), which cannot be directly manipulated using");
		Acidictive.reply(u, to, c, "the plugin command.");
		return true;
	}
}