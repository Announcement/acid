/*
 * Copyright (c) 2016, Darius Jahandarie <djahandarie@rizon.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package net.rizon.acid.util;

public class Format
{
	public static final char BOLD = 2;
	public static final char UNDERLINE = 31;

	public static final char COLOR = 3;
	public static final int RED = 4;
	public static final int ORANGE = 7;
	public static final int YELLOW = 8;
	public static final int BLUE = 10;
	public static final int PINK = 13;
	public static final int GREY = 14;

	public static String color(int code)
	{
		if (code == 0)
		{
			return COLOR + "";
		}
		return COLOR + String.format("%02d", code);
	}

	public static String color(int code1, int code2)
	{
		return COLOR + String.format("%02d", code1) + "," + String.format("%02d", code2);
	}

	public static String wrapIf(boolean bool, String wrap, String inner)
	{
		return bool ? wrap + inner + wrap : inner;
	}

	public static String wrapIf(boolean bool, String before, String after, String inner)
	{
		return bool ? before + inner + after : inner;
	}

	private static String stripFormatting(String s)
	{
		return s.replaceAll("(\\x03[0-9]{1,2}(,[0-9]{1,2})?)|[\\x02\\x03\\x1D\\x1F\\x16\\x0F]", "");
	}

	public static int visibleLength(String s)
	{
		return stripFormatting(s).length();
	}

}
