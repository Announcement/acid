package net.rizon.acid.conf;

public class Database implements Validatable
{
	public String name, host, user, pass;

	@Override
	public void validate() throws ConfigException
	{
		Validator.validateNotEmpty("name", name);
		Validator.validateNotEmpty("host", host);
		Validator.validateNotEmpty("user", user);
		Validator.validateNotEmpty("pass", pass);
	}
}