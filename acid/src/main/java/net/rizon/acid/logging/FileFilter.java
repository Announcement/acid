package net.rizon.acid.logging;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;
import net.rizon.acid.core.Acidictive;

public class FileFilter extends Filter<ILoggingEvent>
{
	@Override
	public FilterReply decide(ILoggingEvent event)
	{
		boolean isFileLogger = Acidictive.fileLogger != null && event.getLoggerName().equals(Acidictive.fileLogger.getName());
		return isFileLogger ? FilterReply.DENY : FilterReply.NEUTRAL;
	}

}
