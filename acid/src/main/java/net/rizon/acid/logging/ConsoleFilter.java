package net.rizon.acid.logging;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;
import net.rizon.acid.core.Acidictive;

public class ConsoleFilter extends Filter<ILoggingEvent>
{
	@Override
	public FilterReply decide(ILoggingEvent event)
	{
		boolean show = (Acidictive.conf != null && Acidictive.conf.debug) || event.getLevel() != Level.DEBUG;;
		return show ? FilterReply.NEUTRAL : FilterReply.DENY;
	}

}
