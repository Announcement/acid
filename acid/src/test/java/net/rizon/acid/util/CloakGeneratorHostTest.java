/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.rizon.acid.util;

import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author Orillion {@literal <orillion@rizon.net>}
 */
@RunWith(Parameterized.class)
public class CloakGeneratorHostTest
{
	private static final String KEY_ONE = "cloakKey1";
	private static final String KEY_TWO = "cloakKey2";
	private static final String KEY_THREE = "cloakKey3";
	private static final String NETWORK = "Rizon-";

	private static final String KEYS[] =
	{
		KEY_ONE, KEY_TWO, KEY_THREE
	};

	private final String host;
	private final String expected;

	private CloakGenerator cloakGenerator;

	public CloakGeneratorHostTest(String host, String expected)
	{
		this.host = host;
		this.expected = expected;
	}

	@Before
	public void setUp()
	{
		this.cloakGenerator = new CloakGenerator(KEYS, NETWORK);
	}

	@After
	public void tearDown()
	{
		this.cloakGenerator = null;
	}

	@Parameterized.Parameters
	public static Collection ips6()
	{
		return Arrays.asList(new Object[][]
		{
			{
				"cakes.net", NETWORK + "77A67854.net"
			},
			{
				"cakes.10.12.net", NETWORK + "F0DDF00E.net"
			},
			{
				"ip42.cakes.10.net", NETWORK + "7D504780.cakes.10.net"
			},
			{
				"1.2.3.4.5.6.7.9.super.google.irc", NETWORK + "DF13E3E9.super.google.irc"
			}

		});
	}

	@Test
	public void testHostCloak()
	{
		String result = this.cloakGenerator.cloak(this.host);
		Assert.assertEquals(this.expected, result);
	}
}
