package net.rizon.acid.util;

import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author Orillion {@literal <orillion@rizon.net>}
 */
@RunWith(Parameterized.class)
public class CloakGeneratorIPv4Test
{
	private static final String KEY_ONE = "cloakKey1";
	private static final String KEY_TWO = "cloakKey2";
	private static final String KEY_THREE = "cloakKey3";
	private static final String NETWORK = "Rizon-";

	private static final String KEYS[] =
	{
		KEY_ONE, KEY_TWO, KEY_THREE
	};

	private final String ipv6;
	private final String expected;

	private CloakGenerator cloakGenerator;

	public CloakGeneratorIPv4Test(String ipv6, String expected)
	{
		this.ipv6 = ipv6;
		this.expected = expected;
	}

	@Before
	public void setUp()
	{
		this.cloakGenerator = new CloakGenerator(KEYS, NETWORK);
	}

	@After
	public void tearDown()
	{
		this.cloakGenerator = null;
	}

	@Parameterized.Parameters
	public static Collection ips6()
	{
		return Arrays.asList(new Object[][]
		{
			{
				"127.0.0.1", "3317843C.232EC1D0.89C2F6D2.IP"
			},
			{
				"127.0.1.1", "4FD42D4.52356747.89C2F6D2.IP"
			},
			{
				"128.0.0.0", "EFB21FC1.CB8E5CE9.45DD05EE.IP"
			},
			{
				"0.0.0.0", "5A50FD90.833E8F76.36201F95.IP"
			},
			{
				"255.255.255.255", "C7C7D066.A18B600F.67F6A496.IP"
			},
			{
				"1.1.1.1", "9F5EF322.5C907051.DA359507.IP"
			},
			{
				"1.2.3.4", "F3F65368.657FBCBB.9E35423B.IP"
			},
			{
				"0.255.0.255", "5CE3D1E6.C5C8CABB.CFF89735.IP"
			}
		});
	}

	@Test
	public void testIPv4Cloak()
	{
		String result = this.cloakGenerator.cloak(this.ipv6);
		Assert.assertEquals(this.expected, result);
	}
}
